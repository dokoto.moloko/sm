use clap::ArgMatches;
use std::error::Error;

use crate::actions;
use crate::args;
use crate::ddbb::Database;
use crate::utils::{
    build_config_dir, 
    build_cache_dir,
    create_xdg_dir_if_not_exist};

pub struct Framework {
    db: Database,
    args: ArgMatches,
}
/*
 * FALTA (faltan mas cosas pero esto seria lo mas importante)
 * TUI para thumbs de videos
 */
impl Framework {
    pub fn constructor() -> Result<Framework, Box<dyn Error>> {
        let args = args::parse();
        let mut my_config_dir = build_config_dir("sm")?;
        let my_cache_dir = build_cache_dir("sm")?;
        create_xdg_dir_if_not_exist(&my_config_dir)?;
        create_xdg_dir_if_not_exist(&my_cache_dir)?;
        my_config_dir.push("sm.db3");
        let db = Database::connect(my_config_dir.to_str().unwrap())?;
        db.create_tables()?;
        Ok(Framework { db, args })
    }

    pub async fn run(&self) -> Result<(), Box<dyn Error>> {
        match self.args
            .value_of("MODE")
            .expect("MODE is required")
            {
                "videos" => self.videos_actions().await?,
                "authors" => self.authors_actions().await?,
                "radios" => self.radios_actions().await?,
                "favorites" => self.favorites_actions().await?,
                _ => println!("hitler")
            }
        Ok(())
    }

    pub fn clean(self)
        -> Result<(), Box<dyn Error>> {
        self.db.disconnect()?;
        Ok(())
    }
    
    async fn favorites_actions(&self)
        -> Result<(), Box<dyn Error>> {
        if self.args.is_present("list") {
            actions::list(&self.db)?;
        }
        if self.args.is_present("delete") {
            let id = self.args.value_of("delete").unwrap();
            actions::delete(&self.db, id)?;
        }
        if self.args.is_present("add_author") {
            let id = self.args.value_of("add_author").unwrap();
            let nocache = self.args.is_present("nocache");
            actions::add_author(&self.db, id, nocache).await?;
        }
        if self.args.is_present("add_video") {
            println!("No implemented yet")
        }
        if self.args.is_present("add_radio") {
            println!("No implemented yet")
        }
        Ok(())
    }

    async fn videos_actions(&self) 
        -> Result<(), Box<dyn Error>> {
        if self.args.is_present("search") {
            let search = self.args.value_of("search").unwrap();
            let nocache = self.args.is_present("nocache");
            if self.args.is_present("download") {
                actions::search_videos_with_images(&self.db, search, nocache).await?;
            } else {
                actions::search_videos(&self.db, search, nocache).await?;
            }
        }
        if self.args.is_present("export") {
            let id = self.args.value_of("export").unwrap();
            let nocache = self.args.is_present("nocache");
            actions::export_video(&self.db, id, nocache).await?;
        }
        if self.args.is_present("get") {
            println!("No Implemented yet")
        }
        
        Ok(())
    }

    async fn authors_actions(&self)
        -> Result<(), Box<dyn Error>> {
        if self.args.is_present("search") {
            let search = self.args.value_of("search").unwrap();
            let nocache = self.args.is_present("nocache");
            actions::search_authors(&self.db, search, nocache).await?;
        }
        if self.args.is_present("last") {
            let id = self.args.value_of("last").unwrap();
            let nocache = self.args.is_present("nocache");
            if self.args.is_present("download") {
                actions::get_latest_with_images(&self.db, id, nocache).await?;
            } else {
                actions::get_latest(&self.db, id, nocache).await?;
            }
        }
        if self.args.is_present("get") {
            let id = self.args.value_of("get").unwrap();
            actions::get_fav_author(&self.db, id)?;
        }
        Ok(())
    }

    async fn radios_actions(&self)
        -> Result<(), Box<dyn Error>> {
        if self.args.is_present("search") {
            let search = self.args.value_of("search").unwrap();
            actions::search_radios(search).await?;
        }
        if self.args.is_present("export") {
            let id = self.args.value_of("export").unwrap();
            actions::export_radio(id)?;
        }
        if self.args.is_present("get") {
            let id = self.args.value_of("get").unwrap();
            actions::get_fav_radio(&self.db, id)?;
        }
        Ok(())
    }
}
