use rusqlite::{params, Connection, Result};
use std::error;
use uuid::Uuid;

#[allow(dead_code)]
pub struct Database {
    pub conn: Connection,
    path: String,
    root_id: String,
}

impl Database {
    pub fn connect(path: &str) -> Result<Database, Box<dyn error::Error>> {
        let conn = Connection::open(path)?;
        let root_id = Uuid::new_v4().to_string();
        Ok(Database {
            conn,
            path: path.to_string(),
            root_id,
        })
    }

    pub fn disconnect(self) -> Result<(), Box<dyn error::Error>> {
        if Connection::close(self.conn).is_err() {
            return (|| return Err("Cant disconnect from ddbb"))()?;
        }
        Ok(())
    }

    pub fn create_tables(&self) -> Result<(), Box<dyn error::Error>> {
        // Favorites 
        self.conn.execute(
            "CREATE TABLE IF NOT EXISTS favorites (
                    id CHARACTER(36) PRIMARY KEY UNIQUE NOT NULL,
                    fav_type VARCHAR(20),
                    label VARCHAR(500),
                    url VARCHAR(2048)
            ) WITHOUT rowid",
            [],
        )?;
        self.conn.execute(
            "CREATE UNIQUE INDEX IF NOT EXISTS idx_fav_id ON favorites (id)",
            [],
        )?;

        // Tags
        self.conn.execute(
            "CREATE TABLE IF NOT EXISTS tags (
                    fav_id CHARACTER(36),
                    label VARCHAR(500) NOT NULL
            )",
            [],
        )?;
        self.conn.execute(
            "CREATE UNIQUE INDEX IF NOT EXISTS idx_label ON tags (label)",
            [],
        )?;
        
        // Radios 
        self.conn.execute(
            "CREATE TABLE IF NOT EXISTS radios (
                id CHARACTER(36) PRIMARY KEY UNIQUE NOT NULL,
                fav_id CHARACTER(36) NOT NULL,
                name VARCHAR(200),
                radio_id VARCHAR(20),
                city VARCHAR(100),
                country VARCHAR(100)
            ) WITHOUT rowid",
            [],
        )?;

        // Videos
        self.conn.execute(
            "CREATE TABLE IF NOT EXISTS videos (
                id CHARACTER(36) PRIMARY KEY UNIQUE NOT NULL,
                fav_id CHARACTER(36) NOT NULL,
                name VARCHAR(200),
                video_id VARCHAR(20),
                author_id VARCHAR(20)
             ) WITHOUT rowid",
            [],
        )?;

        // Authors
        self.conn.execute(
            "CREATE TABLE IF NOT EXISTS authors (
                id CHARACTER(36) PRIMARY KEY UNIQUE NOT NULL,
                fav_id CHARACTER(36) NOT NULL,
                name VARCHAR(200),
                author_id VARCHAR(20)
             ) WITHOUT rowid",
            [],
        )?;

        // instances
        self.conn.execute(
            "CREATE TABLE IF NOT EXISTS instances (
                    host VARCHAR(500) PRIMARY KEY UNIQUE NOT NULL,
                    tag VARCHAR(20) UNIQUE NOT NULL)", []
        )?;
        self.conn.execute(
            "CREATE UNIQUE INDEX IF NOT EXISTS idx_tag ON instances (tag)",
            [],
        )?;
        Ok(())
    }
}
