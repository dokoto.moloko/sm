use std::error;
use rusqlite::params;

use crate::ddbb::Database;

pub struct Instance();

pub struct Favorite {
    pub id: String,
    pub fav_type: String,
    pub label: String,
    pub url: String,
}

pub struct Author {
    pub id: String,
    pub fav_id: String,
    pub name: String,
    pub author_id: String,
}

pub struct Radio {
    pub id: String,
    pub fav_id: String,
    pub name: String,
    pub radio_id: String,
    pub city: String,
    pub country: String,
}

pub enum FavoriteType {
    Videos(String),
    Authors(String),
    Radios(String), 
}

impl Instance {
    pub fn exists(db: &Database)
        -> Result<bool, Box<dyn error::Error>> {
        let mut stmt = db.conn.prepare("SELECT * FROM instances")?;
        let exist = stmt.exists([])?;
        Ok(exist)
    }

    pub fn insert(db: &Database, host: &str, tag: &str)
        -> Result<(), Box<dyn error::Error>> {
        db.conn.execute(
            "INSERT INTO instances (host, tag) VALUES (?1, ?2)",
            params![host, tag],
        )?;
        Ok(())
    }

    pub fn update(db: &Database, host: &str, tag: &str)
        -> Result<(), Box<dyn error::Error>> {
        db.conn.execute(
            "UPDATE instances SET host=?1 WHERE tag=?2",
            params![host, tag],
        )?;
        Ok(())
    }

    pub fn select(db: &Database)
        -> Result<String, Box<dyn error::Error>> {
        let mut stmt = db.conn.prepare("SELECT host FROM instances WHERE tag='top'")?;
        let row = stmt.query_row([], |row| row.get(0))?;
        Ok(row)
    }
}

impl Radio {
    pub fn print_row(radio: &Radio) {
         println!("{}\t{}",
                     radio.radio_id, radio.name);
    }
    
}

impl Favorite {
    pub fn select(db: &Database)
        -> Result<Vec<Favorite>, Box<dyn error::Error>> {
        let mut stmt = db.conn.prepare("SELECT * FROM favorites")?;
        let ch_iter = stmt
            .query_map([], |row| {
                Ok(Favorite {
                    id: row.get(0)?,
                    fav_type: row.get(1)?,
                    label: row.get(2)?,
                    url: row.get(3)?,
                })
            })?;

        let favorites = ch_iter.map(|c| c.unwrap()).collect::<Vec<Favorite>>();

        Ok(favorites)
    }

    pub fn select_radios_row(db: &Database, id: &str)
        -> Result<Radio, Box<dyn error::Error>> {
        let mut stmt = db.conn
            .prepare("SELECT * FROM radios where fav_id=?1")?;
        let radio = stmt
            .query_row([id], |row| {
                Ok(Radio {
                    id: row.get(0)?,
                    fav_id: row.get(1)?,
                    name: row.get(2)?,
                    radio_id: row.get(3)?,
                    city: row.get(4)?,
                    country: row.get(5)?,
                })
            })?;

        Ok(radio)
    }

    pub fn select_authors_row(db: &Database, id: &str)
        -> Result<Author, Box<dyn error::Error>> {
        let mut stmt = db.conn
            .prepare("SELECT * FROM authors where fav_id=?1")?;
        let author = stmt
            .query_row([id], |row| {
                Ok(Author {
                    id: row.get(0)?,
                    fav_id: row.get(1)?,
                    name: row.get(2)?,
                    author_id: row.get(3)?,
                })
            })?;

        Ok(author)
    }

    pub fn print(favorites: Vec<Favorite>) {
        for favorite in favorites {
            println!("{}\t{}\t{}\t{}",
                     favorite.id, favorite.fav_type, favorite.label, favorite.url);
        }
    }

    pub fn delete(db: &Database, id: &str)
        -> Result<(), Box<dyn error::Error>> {
        db.conn
            .execute("DELETE FROM radios WHERE fav_id=?1", params![id])?;
        db.conn
            .execute("delete from videos where fav_id=?1", params![id])?;
        db.conn
            .execute("delete from authors where fav_id=?1", params![id])?;           
        db.conn
            .execute("DELETE FROM favorites WHERE id=?1", params![id])?; 

        Ok(())
    }

    pub fn insert(db: &Database, favorite: &Favorite)
        -> Result<(), Box<dyn error::Error>> {
        db.conn.execute(
            "INSERT OR IGNORE INTO favorites(id, fav_type, label, url)
                VALUES (?1, ?2, ?3, ?4)",
            params![favorite.id, favorite.fav_type, favorite.label, favorite.url] 
        )?;
        Ok(())
    }
}

impl Author {
    pub fn insert(db: &Database, author: &Author)
        -> Result<(), Box<dyn error::Error>> {
        db.conn.execute(
            "INSERT OR IGNORE INTO authors(id, fav_id, name, author_id)
                VALUES (?1, ?2, ?3, ?4)",
            params![author.id, author.fav_id, author.name, author.author_id] 
        )?;
        Ok(())
    }

    pub fn print_row(author: &Author) {
         println!("{}\t{}\t{}\t{}",
                     author.id, author.fav_id, author.name, author.author_id);
    }
}
