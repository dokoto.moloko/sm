use serde::Deserialize;
use regex::Regex;
use std::error;
use reqwest::Client;
use bytes::Bytes;

const INSTANCES_EXCLUDED: [&str; 2] = ["api.invidious.io", "docs.invidious.io"];
const INSTANCES_ENDPOINT: &str = "https://stats.uptimerobot.com/api/getMonitorList/89VnzSKAn?page=1";
const SUCCESS_STATUS: &str = "success";

#[derive(Deserialize, Debug)]
pub struct Radio {
    code: String,
    #[serde(default)]
    subtitle: String,
    title: String,
    url: String,
}

#[derive(Deserialize, Debug)]
pub struct Hit {
    #[serde(rename = "_source")]
    source: Radio,
}

#[derive(Deserialize, Debug)]
pub struct Hits {
    hits: Vec<Hit>,
}

#[derive(Deserialize, Debug)]
pub struct RadiosResp {
    hits: Hits,
}
#[derive(Deserialize, Debug)]
struct Ratio {
    ratio: String,
    label: String,
}

#[derive(Deserialize, Debug)]
struct Monitor {
    name: String,
    #[serde(rename = "dailyRatios")]
    daily_ratios: Vec<Ratio>,
}

#[derive(Deserialize, Debug)]
struct Psp {
    monitors: Vec<Monitor>,
}

#[derive(Deserialize, Debug)]
pub struct Instance {
    psp: Psp,
}

#[derive(Deserialize, Debug)]
pub struct Author {
    #[serde(rename = "author")]
    pub name: String,
    #[serde(rename = "authorId")]
    pub id: String,
    #[serde(rename = "authorUrl")]
    pub author_url: String,
}

#[derive(Deserialize, Debug)]
pub struct Thumbs {
    pub quality: String,
    pub url: String,
    pub width: u32,
    pub height: u32,
}

#[derive(Deserialize, Debug)]
pub struct Video {
    #[serde(rename = "type")]
    ty: String,
    pub title: String,
    #[serde(default)]
    #[serde(rename = "videoId")]
    pub video_id: String,
    #[serde(default)]
    #[serde(rename = "authorId")]
    pub author_id: String,
    #[serde(rename = "lengthSeconds")]
    length_seconds: u32,
    #[serde(default)]
    #[serde(rename = "videoThumbnails")]
    pub video_thumbnails: Vec<Thumbs>,
}

impl Video {
    pub async fn get(name: &str, inv_instance: &str)
        -> Result<Vec<Video>, Box<dyn error::Error>> {
        let url = format!(
            "https://{}/api/v1/search?q={}&type=video",
            inv_instance,
            name);
        let client = Client::new();
        let response = client
          .get(url)
          .header("content-type", "application/json")
          .header("accept", "application/json")
          .send()
          .await?;
        let videos = response.json::<Vec<Video>>().await?;

        Ok(videos)
    }

    pub async fn req_get_thumb(url: String)
        -> Result<Bytes, Box<dyn error::Error>> {
        let client = Client::new();
        let image = client
          .get(url)
          .header("content-type", "image/*")
          .header("accept", "image/*")
          .send()
          .await?
          .bytes()
          .await?;

        Ok(image)
    }

    pub fn print_me(&self) {
        println!("{}\t{}\t{}",
                 self.video_id, self.author_id, self.title);
    }

    pub fn print(videos: &Vec<Video>) {
        for video in videos {
            video.print_me();
        }
    }
}

impl Author {
    pub async fn get(name: &str, inv_instance: &str)
        -> Result<Vec<Author>, Box<dyn error::Error>> {
        let url = format!("https://{}/api/v1/search?q={}&type=channel", inv_instance, name);
        let client = Client::new();
        let response = client
          .get(url)
          .header("content-type", "application/json")
          .header("accept", "application/json")
          .send()
          .await?;
        let authors = response.json::<Vec<Author>>().await?;

        Ok(authors)
    }

    pub async fn get_latest(id: &str, inv_instance: &str)
        -> Result<Vec<Video>, Box<dyn error::Error>> {
        let url = format!("https://{}/api/v1/channels/latest/{}", inv_instance, id);
        let client = Client::new();
        let response = client
          .get(url)
          .header("content-type", "application/json")
          .header("accept", "application/json")
          .send()
          .await?;
        let videos = response.json::<Vec<Video>>().await?;

        Ok(videos)
    }

    pub async fn get_row(id: &str, inv_instance: &str)
        -> Result<Author, Box<dyn error::Error>> {
        let url = format!("https://{}/api/v1/channels/{}", inv_instance, id);
        let client = Client::new();
        let response = client
          .get(url)
          .header("content-type", "application/json")
          .header("accept", "application/json")
          .send()
          .await?;
        let author = response.json::<Author>().await?;

        Ok(author)
    }

    pub fn print(authors: &Vec<Author>) {
        for author in authors {
            println!("{}\t{}",
                     author.id, author.name);
        }
    }
}

impl Instance {
    pub async fn get()
        -> Result<String, Box<dyn error::Error>> {
        let client = Client::new();
        let response = client
          .get(INSTANCES_ENDPOINT)
          .header("content-type", "application/json")
          .header("accept", "application/json")
          .send()
          .await?;
        let inv_instances = response.json::<Instance>().await?;
        let inv_instance: String = inv_instances.psp.monitors
            .iter()
            .filter(|monitor| { 
                    if let Some(first_ratio) = monitor.daily_ratios.get(0) {
                        return INSTANCES_EXCLUDED
                            .iter()
                            .find(|&&i| i == monitor.name.as_str()).is_none()
                        &&first_ratio.label == SUCCESS_STATUS;
                    }
                    false
            })
            .map(|monitor| String::from(&monitor.name))
            .take(1)
            .collect::<String>();

        Ok(inv_instance)
    }
}

fn get_channel_id(channel: &str) -> Option<String> {
    match Regex::new(r#"/[listen|visit]/*.+/(\S+)"#) {
        Ok(rx) => {
            match rx.captures(channel) {
                Some(cap) => {
                    if cap.len() < 2 { return None };
                    Some(String::from(&cap[1]))
                }
                None => None,
            }
        }
        Err(_) => None,
    }
}

impl Radio {
    pub async fn get(name: &str)
        -> Result<Vec<Radio>, Box<dyn error::Error>> {
        let url = format!("https://radio.garden/api/search?q={}", name);
        let client = Client::new();
        let response = client
          .get(url)
          .header("content-type", "application/json")
          .header("accept", "application/json")
          .send()
          .await?;
        let body = response.json::<RadiosResp>().await?;
        let radios = body.hits.hits
            .iter()
            .map(|hit| { 
                let code = get_channel_id(&hit.source.url).unwrap();
                Radio {
                    code,
                    subtitle: hit.source.subtitle.clone(),
                    title: hit.source.title.clone(),
                    url: hit.source.url.clone(),
                }
            })
            .collect::<Vec<Radio>>();
        
        Ok(radios)
    }

    pub fn print(radios: &Vec<Radio>) {
        for radio in radios {
            println!("{}\t{}\t{}",
                     radio.code, radio.title, radio.subtitle);
        }
    }
}
