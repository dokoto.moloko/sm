use std::error;
use std::path::PathBuf;
use std::fs;
use dirs::{config_dir, cache_dir};

pub fn build_config_dir(name: &str)
    -> Result<PathBuf, Box<dyn error::Error>> {
    let mut my_config_dir = config_dir().ok_or("No config dir found")?;
    my_config_dir.push(name);
    let my_config_path = PathBuf::from(&my_config_dir);

    Ok(my_config_path)
}

pub fn build_cache_dir(name: &str)
    -> Result<PathBuf, Box<dyn error::Error>> {
    let mut my_cache_dir = cache_dir().ok_or("No cache dir found")?;
    my_cache_dir.push(name);
    let my_cache_path = PathBuf::from(&my_cache_dir);

    Ok(my_cache_path)
}

pub fn create_xdg_dir_if_not_exist(xdg_dir: &PathBuf)
    -> Result<(), Box<dyn error::Error>> {
    if !xdg_dir.exists() {
        fs::create_dir(&xdg_dir)?;
    }

    Ok(())
}
