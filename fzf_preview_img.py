#!/usr/bin/env python3

import os, sys
from xdg import xdg_cache_home

#ls -1 *.jpg|fzf --preview 'kitty +kitten icat --transfer-mode file --place "${FZF_PREVIEW_COLUMNS}x${FZF_PREVIEW_LINES}@$((FZF_PREVIEW_COLUMNS+6))x1" --scale-up {}'
def main():
    id, _, _ = sys.argv[1].split('\t')
    cache_file = f'{xdg_cache_home()}/sm/{id}.jpg'
    os.system('kitty +kitten icat --transfer-mode file --place '\
        '"${FZF_PREVIEW_COLUMNS}x${FZF_PREVIEW_LINES}@$((FZF_PREVIEW_COLUMNS+6))x1" '\
        '--scale-up ' + cache_file)

if __name__ == '__main__':
   main()
