#!/usr/bin/env python3

import os
from pathlib import Path

LOCAL_BIN = f'{Path.home()}/.local/bin/'
print('Building and installing sm')
os.system('cargo install --path . 2> /dev/null')
print(f'Coping media.py to {LOCAL_BIN}media as media binary')
os.system(f'cp media.py {LOCAL_BIN}media')
print(f'Coping fzf_preview_img.py to {LOCAL_BIN}media as media binary')
os.system(f'cp fzf_preview_img.py {LOCAL_BIN}fzf_preview_img')

